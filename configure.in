dnl Process this file with autoconf to produce a configure script.

AC_INIT(configure.in)
AM_INIT_AUTOMAKE(aptivate, 0.1)
AM_CONFIG_HEADER(config.h)

dnl Add the languages which your application supports here.
ALL_LINGUAS="de no ru sv"

dnl Pick up the Gnome macros.
AM_ACLOCAL_INCLUDE(macros)

AM_PROG_LIBTOOL
AM_SANITY_CHECK

dnl GNOME_INIT
AC_ISC_POSIX
AC_PROG_CC
AC_PROG_CXX
AM_PROG_CC_STDC
AC_HEADER_STDC

#
# Find pkg-config
#
AC_PATH_PROG(PKG_CONFIG, pkg-config, no)
if test x$PKG_CONFIG = xno ; then
  AC_MSG_ERROR([*** pkg-config not found. See http://pkgconfig.sourceforge.net])
fi

if ! pkg-config --atleast-pkgconfig-version 0.5 ; then
  AC_MSG_ERROR([*** pkg-config too old; version 0.5 or better required.])
fi

#
# Checks for GLib
#
GLIB_PACKAGES="gobject-2.0 gmodule-2.0"
GLIB_REQUIRED_VERSION=1.3.2

AC_MSG_CHECKING(GLib version)
if $PKG_CONFIG --atleast-version $GLIB_REQUIRED_VERSION glib-2.0 ; then
      GLIB_CFLAGS=`$PKG_CONFIG --cflags $GLIB_PACKAGES`
      GLIB_LIBS=`$PKG_CONFIG --libs $GLIB_PACKAGES`
      GLIB_VERSION=`$PKG_CONFIG --modversion glib-2.0`

      AC_MSG_RESULT($GLIB_VERSION)
else
      AC_MSG_ERROR([
*** GLIB $GLIB_REQUIRED_VERSION or newer is required. The latest version of GLIB
*** is always available from ftp://ftp.gtk.org/.
  ])
fi
AC_SUBST(GLIB_LIBS)
AC_SUBST(GLIB_CFLAGS)

#
# Checks for GTK
#
GTK_PACKAGES="gtk+-2.0"
GTK_REQUIRED_VERSION=1.3.2

AC_MSG_CHECKING(Gtk+ version)
if $PKG_CONFIG --atleast-version $GTK_REQUIRED_VERSION gtk+-2.0 ; then
      GTK_CFLAGS=`$PKG_CONFIG --cflags $GTK_PACKAGES`
      GTK_LIBS=`$PKG_CONFIG --libs $GTK_PACKAGES`
      GTK_VERSION=`$PKG_CONFIG --modversion gtk+-2.0`

      AC_MSG_RESULT($GTK_VERSION)
else
      AC_MSG_ERROR([
*** GTK $GTK_REQUIRED_VERSION or newer is required. The latest version of GTK
*** is always available from ftp://ftp.gtk.org/.
  ])
fi
AC_SUBST(GTK_LIBS)
AC_SUBST(GTK_CFLAGS)

AC_ARG_ENABLE(gtk_enable_broken, [  --enable-gtk-broken     use depricated gtk functions ],,gtk_enable_broken=yes)
AC_DEFINE(GTK_ENABLE_BROKEN)

#-------------------------------------------------------------------
AM_GNU_GETTEXT

dnl Set PACKAGE_LOCALE_DIR in config.h.
if test "x${prefix}" = "xNONE"; then
  AC_DEFINE_UNQUOTED(PACKAGE_LOCALE_DIR, "${ac_default_prefix}/${DATADIRNAME}/locale")
else
  AC_DEFINE_UNQUOTED(PACKAGE_LOCALE_DIR, "${prefix}/${DATADIRNAME}/locale")
fi

AC_OUTPUT([
Makefile
macros/Makefile
lib/Makefile
lib/tests/Makefile
src/Makefile
intl/Makefile
po/Makefile.in
])
