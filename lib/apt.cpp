#include <stdio.h>
#include <iostream>
#include <string>
#include <apt-pkg/init.h>
#include <apt-pkg/error.h>
#include <apt-pkg/fileutl.h>
#include <apt-pkg/cmndline.h>
#include <apt-pkg/configuration.h>
#include <apt-pkg/pkgsystem.h>
#include <apt-pkg/mmap.h>
#include <apt-pkg/pkgcache.h>
#include <apt-pkg/pkgrecords.h>
#include "apt.h"

pkgCache *cgCache = 0;

int opencache()
{
  if (pkgInitConfig(*_config) == false) return 0;
  if (pkgInitSystem(*_config, _system) == false) return 0;

  FileFd *fd = new FileFd(_config->FindFile("Dir::Cache::pkgcache"),
                          FileFd::ReadOnly);

  MMap *map = new MMap(*fd, MMap::Public|MMap::ReadOnly);
  if (_error->PendingError() == true) {
    _error->DumpErrors();
    return 0;
  }

  cgCache = new pkgCache(map);
  if (0 == cgCache) return 0;
  if (_error->PendingError() == true) {
    _error->DumpErrors();
    return 0;
  }

  return 1;
}

aptPackageRet* getPackages()
{
  aptPackage *pack;
  GList      *list;
  GList      *pos;
  GHashTable *packmap;
  aptPackageRet *ret;
  int        *idp;
  gboolean    avail;

  pkgCache &cache = *cgCache;
  pkgRecords Recs(cache);

  list = g_list_alloc();
  packmap = g_hash_table_new(g_int_hash, g_int_equal);

  idp = (int *)g_malloc(sizeof(int));

  for (pkgCache::PkgIterator I = cache.PkgBegin(); !I.end(); I++) {
    avail = false;

    for (pkgCache::VerIterator V = I.VersionList(); V.end() == false; V++) {
      pkgRecords::Parser &P = Recs.Lookup(V.FileList());
      pack = (aptPackage *)g_malloc0(sizeof(aptPackage));

      pack->name = g_strdup(I.Name());
      pack->filename = g_strdup(V.FileList().File().FileName());
      pack->size = V->Size;
      pack->id   = I->ID;
      pack->version   = g_strdup(V.VerStr());
      pack->shortdesc = g_strdup(P.ShortDesc().c_str());
      pack->longdesc  = g_strdup(P.LongDesc().c_str());

      pack->depends = g_list_alloc();

      for (pkgCache::DepIterator D = V.DependsList(); D.end() == false; D++) {
        aptDep *dep = (aptDep*) g_malloc0(sizeof(aptDep));

        dep->id = D.TargetPkg()->ID;

        pack->depends=g_list_prepend(pack->depends, dep);
      }

      g_list_append(list, pack);
      g_hash_table_insert(packmap, &(I->ID), pack);
      avail = true;
    }

    if (!avail) {
      pack = (aptPackage *)g_malloc0(sizeof(aptPackage));
      pack->name = g_strdup(I.Name());
      pack->filename = g_strdup("");
      pack->size = -1;
      pack->id   = I->ID;
      pack->version = g_strdup("");
      pack->depends = 0;

      g_list_append(list, pack);
      g_hash_table_insert(packmap, &(I->ID), pack);
    }
  }

  ret = (aptPackageRet*)g_malloc0(sizeof(aptPackageRet));
  ret->list = list;
  ret->map  = packmap;

  return ret;
}

void show()
{
  pkgCache &cache = *cgCache;
  string s;

  s = "";
  pkgRecords Recs(cache);
  for (pkgCache::PkgIterator I = cache.PkgBegin(); !I.end(); I++) {
    for (pkgCache::VerIterator V = I.VersionList(); V.end() == false; V++) {
      pkgRecords::Parser &P = Recs.Lookup(V.FileList());
      cout << I.Name() << " " << I->ID << " " << I.Section()  << " "
           << V.FileList().File()->Size << " "
           << V.VerStr() << " "
	   << P.LongDesc() << " "
           << endl;
      for (pkgCache::DepIterator D = V.DependsList(); D.end() == false; D++)
        cout << "  Depends: " << D.TargetPkg().Name() << ' ' << D.TargetVer() 
             << "  " << D.TargetPkg()->ID
             << endl;
    }
    cout << "---------" << endl;
  }
}
