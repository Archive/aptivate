// -*- C++ -*-
#ifndef APT_H
#define APT_H

#ifdef __cplusplus
extern "C" {
#endif

#include <glib.h>

  typedef struct aptPackageRet {
    GList      *list;
    GHashTable *map;
  } aptPackageRet;

  typedef struct aptPackage {
    char  *name;
    char  *filename;
    char  *version;
    int    id;
    int    size;

    char  *longdesc;
    char  *shortdesc;

    GList *depends;
  } aptPackage;

  typedef struct aptDep {
    int    id;
    
  } aptDep;

  int opencache();
  aptPackageRet *getPackages();

  void show();

#ifdef __cplusplus
}
#endif

#endif
