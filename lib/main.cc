#include <iostream>
#include <vector>
#include <string>
#include <stdio.h>
#include "pkgInfo.h"
#include "rdPkgCache.h"

int main (int argc, char **argv) {
  pkgInfo info;
  rdPkgCache apti;
  std::vector<pkgInfo> blah = apti.getPackages();
  for (std::vector<pkgInfo>::iterator iter = blah.begin(); iter != blah.end(); ++iter) {
    info  = *iter;
    cout << "Name:\t\t" << info.name << endl;
    cout << "Version:\t" << info.version << endl;
    cout << "Description:\t" << info.shortDesc << endl;
  }
}
