// -*- C++ -*-
#ifndef PKGINFO_H
#define PKGINFO_H

#include <stdio.h>
#include <iostream>
#include <string>

    class pkgInfo {

        public:
        std::string name;
        std::string filename;
        std::string version;
        std::string shortDesc;
        std::string longDesc;
        std::string depName;
        std::string depVer;
        //std::string depId_str;
        //std::string size_str;
        //std::string id_str;
        int size;
        int depId;
        int id;
    };

#endif
