/* rdPkgCache.cc
 * Class for reading debian's package cache
 * Initial C version written by Thomas Ziehmer <ziehmer@rhrk.uni-kl.de>
 * Extended C++ version written by Christian Meyer <chrisime@gnome.org>
 */

#include "rdPkgCache.h"
#include "pkgInfo.h"

using namespace std;

    rdPkgCache::rdPkgCache () {

        string s = "";

        if (pkgInitConfig (*_config) == false) {
            cout << "Error: Couldn't initialize package system!";
        }
        if (pkgInitSystem (*_config, _system) == false) {
            cout << "Error: Couldn't initiliaze package system!";
        }

        FileFd *fd = new FileFd (_config->FindFile ("Dir::Cache::pkgcache"),
                                 FileFd::ReadOnly);

        MMap *map = new MMap (*fd, MMap::Public|MMap::ReadOnly);
        if (_error->PendingError () == true) {
            _error->DumpErrors ();
        }

        cgCache = new pkgCache (map);
        if (cgCache == 0) {
            cout << "An error occured! Aborting.";
        }
        if (_error->PendingError () == true) {
            _error->DumpErrors ();
        }
    }

    rdPkgCache::~rdPkgCache () {
        cout << "DEBUG: Desctructor called...\n";
    }


    rdPkgCache::ret_vector rdPkgCache::getPackages (void) {

        pkgInfo package;
        pack_vector ret_pkg;
        pkgRecords Recs (*cgCache);

        for (pkgCache::PkgIterator I = cgCache->PkgBegin(); !I.end(); I++) {
            avail = false;

            for (pkgCache::VerIterator V = I.VersionList(); V.end() == false; V++) {
                pkgRecords::Parser &P = Recs.Lookup (V.FileList());

                package.name      = I.Name();
                package.filename  = V.FileList().File().FileName();
                package.size      = V->Size;
                package.id        = I->ID;
                package.version   = V.VerStr();
                package.shortDesc = P.ShortDesc().c_str();
                package.longDesc  = P.LongDesc().c_str();
                package.size = V->Size;

                for (pkgCache::DepIterator D = V.DependsList(); D.end() == false; D++) {
                    package.depName   = D.TargetPkg().Name();
                    //package.depVer    = D.TargetVer();
                }

                ret_pkg.push_back (package);

                avail = true;
            }

            if (!avail) {
                package.name      = I.Name();
                package.filename  = "";
                package.size      = 0;
                package.id        = I->ID;
                package.version   = "";
                package.depName   = "";
                //package.depVer    = "";
                package.depId     = -1;

                ret_pkg.push_back (package);
            }

        }

        return ret_pkg;
    }

    void rdPkgCache::show (void) {
	int count=0;

        pkgRecords Recs(*cgCache);
        for (pkgCache::PkgIterator I = cgCache->PkgBegin(); !I.end(); I++) {
            count++;
            for (pkgCache::VerIterator V = I.VersionList(); V.end() == false; V++) {
                pkgRecords::Parser &P = Recs.Lookup(V.FileList());
                cout << I.Name() << " " << I->ID << " " << I.Section()  << " "
                << V.FileList().File()->Size << " "
                << V.VerStr() << " "
                << P.LongDesc() << " "
                << endl;
                for (pkgCache::DepIterator D = V.DependsList(); D.end() == false; D++)
                    cout << "  Depends: " << D.TargetPkg().Name() << ' ' << D.TargetVer() 
                    << "  " << D.TargetPkg()->ID
                    << endl;
            }
            cout << "---------" << endl;
        }
        cout << "Number of packages: " << count << endl;
    }
