#ifndef APT_H
#define APT_H

#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>
#include <apt-pkg/init.h>
#include <apt-pkg/error.h>
#include <apt-pkg/fileutl.h>
#include <apt-pkg/cmndline.h>
#include <apt-pkg/configuration.h>
#include <apt-pkg/pkgsystem.h>
#include <apt-pkg/mmap.h>
#include <apt-pkg/pkgcache.h>
#include <apt-pkg/pkgrecords.h>
#include "pkgInfo.h"

    class rdPkgCache {

        private:
        typedef std::vector<pkgInfo> pack_vector;

        pkgCache *cgCache;
        bool avail;

        public:
        rdPkgCache ();
        ~rdPkgCache ();
        typedef std::vector<pkgInfo> ret_vector;

        ret_vector getPackages (void);
        void show (void);
};

#endif
