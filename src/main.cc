#include <gtkmm.h>
#include <libglademm/xml.h>
#include <libgnomemm.h>
#include <libgnomeuimm.h>

int main (int argc, char **argv)
{
    Gtk::Main kit (argc, argv);

    Glib::RefPtr<Gnome::Glade::Xml> apt = Gnome::Glade::Xml::create ("../aptivate.glade");
    Gnome::UI::App *aptivate = (Gnome::UI::App *)apt->get_widget ("aptivate");
    Gnome::UI::About *apt_about = (Gnome::UI::About *)apt->get_widget ("aptivate_about");

    //apt_about->hide();

    if (apt_about != 0)
    {
        apt_about->raise();
    }

    // aptivate->set_default_size (800, 600); <-- doesn't work
    aptivate->show ();
    kit.run ();
}

